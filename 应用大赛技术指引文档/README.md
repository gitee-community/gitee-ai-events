### Gitee AI 创新应用大赛·技术指引

本次活动主页地址: https://ai.gitee.com/events/iluvatar-ai-app-contest

参赛报名通过审核后，参赛期间将获得 “天数智芯” 显卡免费使用权，
配置包括 32GB 显存 · 12 核 CPU · 32GB 内存 · 60GB 临时存储，您需要使用此配置参与应用大赛。

####  Gitee AI 应用简介

- Gitee AI 应用引擎可以自由编写任何程序，您可以使用 Python、JavaScript 或 Go、Java、Ruby、PHP、Rust、C++ 等任何编程语言（使用 Docker）来构建您的应用。

- 借助 AI 模型能力，您可以构建一个创意十足、功能强大的 Web 程序。您可以通过 transformers 、diffusion 等库加载 Gitee AI 模型，也可以使用 HTML、JS 等任意编程语言构建界面、调用 Gitee AI 模型引擎 或其他渠道提供的 API 服务。

- 在线部署后，可通过浏览器直接访问、分享您的应用、推广您自己的模型、创意，而无需考虑服务器、算力资源、域名、部署等复杂繁琐问题。

#### 如何创建应用参赛

##### 方法一：直接创建应用

进入 https://ai.gitee.com/apps/new 页面填写应用信息、选择合适的 **SDK**

目前 SDK 有四大类：
- Gradio：您的应用将预设一个 Gradio，它是一个 Python 库，常用于快速构建 AI 应用的用户界面。 您的程序需要 app.py 作为入口，并运行在 7860 端口。

- Streamlit：您的应用将预设一个 Streamlit，它是一个 Python 库，用于快速构建数据应用的用户界面。您的程序需要 app.py 作为入口，并运行在 7860 端口。

- Docker：您可以自定义 Dockerfile 来构建您的应用，自定义环境。您的程序需要运行在 7860 端口。
  
- Static：纯前端的浏览器程序，您可以上传静态文件，如 HTML、JS、CSS 等，以 index.html 作为程序入口，常用于使用 JS 调用 API 服务、Transformers.js 等构建 AI 应用。

> 更多信息参考文档 Gitee AI 应用快速上手：https://ai.gitee.com/docs/getting-started/app

##### 方法二：创建模型引擎
> Gitee AI 模型引擎能将 AI 模型转为 生产级别 API 服务，使得用户可以通过 简单的 API 请求调用模型，将 AI 能力集成到自己的程序中，而无需关心 AI 模型的部署、维护等问题。

- 方法一：进入 https://ai.gitee.com/endpoints/new 页面搜索模型创建
- 方法二：进入 https://ai.gitee.com/models 找到合适模型，模型页面右上角点击 “部署”

然后通过调用 API ，将 AI 能力集成到自己的网站、小程序、APP、扩展插件等产品中。

> 更多信息参考文档模型引擎快速上手 https://ai.gitee.com/docs/getting-started/model

> 您可以在此优先选择天数智芯已验证适配的模型: https://ai.gitee.com/topics/iluvatar
> 也可参考 https://ai.gitee.com/apps 中，使用算力的应用代码

您可以参考组合使用方式，选择一种：

1. 直接创建应用，在应用使用显卡算力
2. 创建模型引擎，创建应用（CPU 套餐），在应用中调用模型引擎 API
3. 创建模型引擎，在你的网站、小程序、APP、扩展插件等产品中调用模型引擎 API
#### 免费算力、环境信息

- 显存：32GB
- CPU：12 核
- 内存：32GB
- 临时存储：60GB

应用引擎主要内置软件信息（应用大赛）：

- 系统：Ubuntu 20.04
- python： 3.10.12
- torch：2.1.0+corex.3.2.0
- vllm：0.2.6+corex.3.2.0
- tensorflow：2.12.0+corex.3.2.0
- paddlepaddle：2.4.1+corex.3.2.0
- diffusers: 0.22.0
- gradio：4.26.0
- git：2.34.1

#### 注意事项

- SDK 选择 Docker，使用 Dockerfile 时请使用以下基础镜像，方可正常使用天数智芯算力。

```Dockerfile
FROM registry.gitee-ai.local/base/iluvatar-corex:3.2.0-bi100
```
- SDK 环境中，默认已内置 torch 2.1.0+corex.3.2.0 等天数智芯定制软件，您可以在此基础上正常运行您的应用，如果您覆盖软件版本，无法保障模型可正常运行。

- 创建 Gitee AI 应用，可以更自由选择模型，某些模型可能需要您适当的调整代码、参数、依赖包版本方可正常运行。
  

#### 问答
- 如何安装依赖？
  - SDK 为 Gradio 则将 python 依赖写入 requirements.txt 文件至仓库根目录即可。前文中内置软件无需重复安装。
- 如何安装系统软件？
  - 例如安装 NodeJs，根目录写入 packages.txt 文件，重启应用将会自动使用 apt-get 安装。
  ```packages
  nodejs
  npm
  ```
> SDK 为 Docker 时，将完全由您自定义环境， 需要您自己执行相关操作，为正常使用算力，需要使用 registry.gitee-ai.local/base/iluvatar-corex:3.2.0-bi100 作为基础镜像

- 应用中如何下载模型？
  使用 transformers、diffusers 、huggingface_hub 等库载入模型将会从 Gitee AI 高速下载，原理是这些库使用的 `HF_ENDPOINT` 环境变量指向了 Gitee AI 内部服务。您也可以使用其他下载方式，例如 Git、魔搭等。

  格式一般为 `hf-models/模型名`：

  - transformers：
  ```python
    from transformers import AutoModelForCausalLM
    model = AutoModelForCausalLM.from_pretrained(
          "hf-models/glm-4-9b-chat",
          torch_dtype=torch.float16,
          low_cpu_mem_usage=True,
          trust_remote_code=True,
        ).to(0).eval()
  ```
  - huggingface_hub，下载 sdxl-turbo 的 sd_xl_turbo_1.0.safetensors 文件到 ./models/checkpoints 目录：
  ```python
    from huggingface_hub import hf_hub_download
    hf_hub_download("hf-models/sdxl-turbo", "sd_xl_turbo_1.0.safetensors", local_dir="./models/checkpoints")
  ```
  - diffusers：
  ```python
    from diffusers import DiffusionPipeline
    DiffusionPipeline.from_pretrained("hf-models/sdxl-turbo", variant="fp16", use_safetensors=True, torch_dtype=torch.float16)
  ```


- 如何使用环境变量、秘钥？
  - 在应用设置-功能中，您可以添加环境变量，使用方法参考：
  https://ai.gitee.com/docs/apps/qa#%E5%A6%82%E4%BD%95%E5%9C%A8%E5%BA%94%E7%94%A8%E4%B8%AD%E4%BD%BF%E7%94%A8%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F%E7%A7%98%E9%92%A5

- 如何让团队成员有权限推送代码到仓库？
  - 应用设置 “成员管理” 可前往 Gitee 添加仓库成员。
  如果使用 SSH 方式推送代码，需要 Gitee 用户添加 SHH 公钥 https://gitee.com/profile/sshkeys
- 创建的应用，Web 界面太小，如何全屏？
  - 应用设置 - 功能 - 应用 Header 布局中可以设置布局。 



前往 Gitee AI 文档了解更多：

应用快速上手： https://ai.gitee.com/docs/getting-started/app

应用问答：https://ai.gitee.com/docs/apps/qa

可参考应用 Gitee AI 运行中的应用：https://ai.gitee.com/apps
