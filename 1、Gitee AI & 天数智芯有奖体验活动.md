![!\[输入图片说明\](images/dalaping.jpg)](images/OSC大拉屏1200x300(1).png)

### 欢迎参与 Gitee AI & 天数智芯有奖体验活动！


随着“AI 时代”的到来，开发者们面临着前所未有的挑战和机遇。为了更好地服务开发者们，Gitee 在一年紧锣密鼓的筹备工作后，宣布 Gitee AI 模型平台正式内测上线——


[立即体验](https://ai.gitee.com/)


Gitee AI 平台是一个旨在让开发者更轻松地探索、使用和部署人工智能模型的创新平台。在这里，您可以体验：丰富的开源模型、天数智芯-智铠通用 GPU、易用的模型推理和应用。

惊蛰时节，Gitee AI 联合天数智芯推出有奖体验活动！参与活动，体验 AI 的魅力，您将有机会赢取丰厚精美礼品！ 



### 活动信息



-  **活动主题：** Gitee AI & 天数智芯有奖体验活动


-  **用户参与时间：** 3月5日-3月29日


-  **名单公布时间：** 3月11日、3月18日、3月25日、4月1日于社群内公布上周获奖名单，并发放 Gitee 站内信。上榜的用户请查收 [Gitee 站内信](https://gitee.com/notifications/messages/)，根据站内信指引在5天内（截至到站内信发布的当周周五）填写并提交个人收货信息。


-  **发奖时间：** 我们将在站内信发布的第二周尽快为填写收货信息的用户发放奖品。



###  参与步骤

1. 点击下方链接，填写问卷。问卷结束后会弹出小助手二维码，扫码添加。


   问卷地址：https://www.lediaocha.com/r/6nk1v


2. 添加后，小助手将邀请您进入活动专属微信群，并根据用户添加顺序在活动期间（3月5日-3月29日）依次派发内测码。


3. 用户获得内测码后，进入 [Gitee AI](http://ai.gitee.com/)，选择【模型】,可在【天数智芯】专区进行模型体验，也可在【模型列表】选择感兴趣的模型进行模型盲测。


*禁止在测试过程中使用一些敏感内容或者诱导生成敏感内容，否则将被封号处理。


###  活动激励
|  活动赛道  | 奖项名称   |               获奖要求                                           | 人数           |
|:------------:|:---------:|:------------------------------------------------------------:|:--------------:|
| 赛道一  | 天数智芯幸运奖 | 获得内测码当周，在天数智芯专区内成功使用天数智芯-智铠通用 GPU 完成  **在线体验/模型引擎**  模型运行数量的前25名用户（运行成功数目相同时，将对比谁更早完成），将获得由 Gitee 提供的全套周边礼包 | 25人/周，共计100人 |
| 赛道二  | 千模盲测幸运奖   | 获得内测码的当周，对非天数智芯专区内的模型使用天数智芯-智铠通用 GPU 完成  **在线体验/模型引擎**  运行数量的前25名用户（运行成功数目相同时，将对比谁更早完成），将获得由天数智芯提供的百元京东卡  | 25人/周，共计100人 |



*以上奖品信息仅供参考，具体礼品以实际收到的实物为准；上述福利奖品兑换有效期至2024年3月31日截止；本次有奖活动最终解释权归活动主办方所有。



###  两大活动赛道


您可以选择下列任意活动赛道进行体验，也可以同时参加。每个赛道将独立排名评奖。


*由于模型体积较大，创建模型引擎时加载耗时较长，属于正常现象（部分模型加载时间预计半小时以上）。请耐心等待~

*此文档以 Stable-diffusion-2、chatyuan-large-v2 两个模型为例，带您探索 Gitee AI 上模型体验使用之旅。

 **赛道①：【天数智芯】专区模型体验**  


 1. 进入天数智芯专区

<img src="images/1.png">


 2. 进入其中任意感兴趣的模型详情页


<img src="images/2.png">


 3. 在天数智芯专区成功运行*在线体验挂件*

<img src="images/3.png">


 4. 您也可以在天数智芯专区创建*模型引擎*，通过模型引擎成功运行该模型
 ![alt text](images/1.2.png)

 ![alt text](images/1.3.png)

![alt text](images/1.4.png)

![alt text](images/1.5(2).png)

5.模型成功运行后（模型生成对应的效果），后台将记录您的成功运行模型的数量信息，并根据该结果排名评奖

*您也可以截图留存模型的成功运行界面（参考赛道①中，3、4步骤中的截图），在对排名有争议时出示截图证明


 **赛道②：千模盲测**  

 1. 进入模型列表

为了提升挑战的成功率，您可以通过以下两种方式进入模型列表选择模型：

 **方法一：**  [筛选功能](http://ai.gitee.com/models?library=transformers%2Cdiffusers&p=1 ) 选择搭载挂件的模型；

![alt text](images/筛选.png) 

 **方法二：** 我们为以下组织/企业下支持在线体验的部份模型提供了内网镜像加速，您也可以优先尝试下面这些组织/企业的模型：


| 组织/企业       | 地址                                           |
|-------------|----------------------------------------------|
| HuggingFace | https://ai.gitee.com/huggingface/huggingface |
| Facebook    | https://ai.gitee.com/huggingface/facebook    |
| Microsoft   | https://ai.gitee.com/huggingface/microsoft   |
| Google      | https://ai.gitee.com/huggingface/google      |
| StabilityAI | https://ai.gitee.com/huggingface/stabilityai |
| OpenAI      | https://ai.gitee.com/huggingface/openai      |


<img src="images/1.6.png">


 2. 进入其中任意未收录进天数智芯专区的模型详情页

<img src="images/2.1.png">

<img src="images/2.2.jpg">



 3. 在千模盲测专区使用【天数智芯】成功运行*在线体验挂件*


<img src="images/3.1.png">


 4. 您也可以在千模盲测专区创建*模型引擎*，通过模型引擎成功运行该模型


<img src="images/4.1.png">

<img src="images/4.2.png">

<img src="images/4.3.png">

<img src="images/4.4.png">


5.模型成功运行后（模型生成对应的效果），将自动分类至天数智芯专区。同时，后台将记录您的成功运行模型的数量信息，并根据该结果排名评奖

*您也可以截图留存模型的成功运行界面（参考赛道②中，3、4步骤中的截图），在对排名有争议时出示截图证明 


 **注意：** 由于算力有限，为了给您带来更好的体验，请不要同时占用2个及以上的模型引擎。我们强烈建议您在成功部署并体验模型之后删除引擎，感谢您的支持与理解~


 操作步骤：

<img src="images/13.png">

<img src="images/14.png">


### 获奖名单公示

 **赛道一：【天数智芯幸运奖】获奖用户Gitee 用户名** 

第一周获奖用户名单

| joyfully_w | llzzll9     | crwth        | gavincfm       | a-strong-python |
|:------------:|:-------------:|:--------------:|:----------------:|:-----------------:|
| yzddczc    | wyb1026     | cancha       | pro_learner    | sam9029         |
| codemix    | w6217858    | czh_36       | neverchanges   | afldt_admin     |
| whabc100   | xuguangjian | wwh123456789 | pinghailinfeng | niceNASA        |
| z_chunyu   | wuZhongtian | no39         | icarusion      | xiaobai00001    |


第二期获奖用户名单
|heanny|imalasong|fy_12|xiao555|chendaiming|
|:--:|:--:|:--:|:--:|:--:|
|frozenm|jinfeng_2_0|kabishou22|apple_green|KennyFocus|
|ShaGuaAiDaTou|simple_wind|nuo-yunkai|li__zhuo|dongchen1020|
|shengweishengwei|yinxx|hapboy|luoxi-c|nuj|
|shuanglinshe|zhuyoubin|qq343509740|zelian|cmdd|


第三期获奖用户名单
| linjian19811027_admin | solitarow     | gzbkey       | xuxianys      | citymoon             |
|:-----------------------:|:---------------:|:--------------:|:---------------:|:----------------------:|
| bestach               | zheng-zizhi   | uimoa        | lbyyds2003    | hu-qi                |
| Amelime               | happylxl      | buguai_lxw   | m3520573159   | mengxiangjiamen      |
| D12121                | zhaojianchao6 | taoshengling | sillykid666   | superziyear          |
| incense-of-shangguan  | haitunhv      | chen-dilu    | summer-grape6 | yang_shan_qing_admin |

第四期获奖用户名单
| 列1                      | 列2                      | 列3                      | 列4                      | 列5                      |
| :------------------------: | :------------------------: | :------------------------: | :------------------------: | :------------------------: |
| bellamour                | xiaochangbai             | to2in1                   | shuaiyidian              | haiyu888                 |
| AB7723                   | projectoasis             | titanos                  | i8a8888a                 | liyiyi12345              |
| zhangqianglovec          | christina-dong           | liu-yifei12345           | liu_jojy                 | ilovea                   |
| buyeach                  | xyttaotao                | unsun                    | wangsisi666              | zheng-zizhi1             |
| Ai-Haibara               | gong-han-lin             | mkq229                   | wejoker                  | pacific-seabird-limited_0|
| linjian19811027_admin    | mirxiong                 |                          |                          |                          |

 **赛道二：【千模盲测幸运奖】获奖用户Gitee 用户名** 

第一周获奖用户名单

| crwth       | gavincfm   | a-strong-python | pro_learner    | cancha              |
|:-------------:|:------------:|:-----------------:|:----------------:|:---------------------:|
| afldt_admin | z_chunyu   | liwen           | pinghailinfeng | dongchen1020        |
| icarusion   | ichenxiang | liu_jojy        | jackhunx       | eucalyptus-universe |
| Shoestrong  | HTKing     | wwy321          | xiaochangbai   | Jeffiy              |


第二期获奖用户名单
| heanny     | imalasong   | fy_12      | xiao555     | chendaiming |
|:------------:|:-------------:|:------------:|:-------------:|:-------------:|
| frozenm    | jinfeng_2_0 | kabishou22 | apple_green | KennyFocus  |
| czc2       | joyfully_w  | gao-zhanr  | quyan-0618  | bbw001      |
| eleven0220 | mia2046     | shooa      |             |             |

第三期获奖用户名单
| uimoa         | linjian19811027_admin | happylxl    | solitarow     | buguai_lxw     |
|:---------------:|:-----------------------:|:-------------:|:---------------:|:----------------:|
| sillykid666   | citymoon              | to2in1      | summer-grape6 | SupperMan-1997 |
| ShaGuaAiDaTou | fenglegend            | zheng-zizhi | luoxi-c       | lbyyds2003     |


第四期获奖用户名单
| 列1                  | 列2                  | 列3                  | 列4                  | 列5                  |
| :--------------------: | :--------------------: | :--------------------: | :--------------------: | :--------------------: |
| i8a8888a             | haiyu888             | shuaiyidian          | ilovea               | simple_wind          |
| bellamour            | D12121               | llzzll9              | projectoasis         | Ai-Haibara           |
| look-uncle-ha_0      | christina-dong       | titanos              | mirxiong             | wangsisi666          |
| Amelime              | Jesai                | liu-yifei12345       | test-user-fission---shure_0   | yyaayaya             |
| PikeLiang            | zssmumu              |                      |                      |                      |		
 
**参与活动，赢取精美礼品！** 



 **AI时代已至，Gitee AI 与您携手迎接未来！** 

*最终解释权归主办方所有