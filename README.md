![输入图片说明](images/2/%E5%BA%94%E7%94%A8%E5%A4%A7%E8%B5%9B%20OSC%E7%A4%BE%E5%8C%BA%E6%B4%BB%E5%8A%A8%E8%BD%AE%E6%92%AD%E5%9B%BE(1).jpg)

# 活动主题
逐梦 AI，赢取未来 

Gitee AI & 天数智芯创新应用大赛 

20万奖池，等你挑战


# 大赛简介

“ AI 时代已经开始：大模型 AI 像 iPhone 和互联网一样具备划时代的革命性。”在这个时代背景下，开发者该如何与 AI 协同进化，让 AI 工具赋能工作生活，提升个人能力和价值，成为了立足之本。为了帮助开发者们在 AI 的浪潮中稳健前行，Gitee AI 联合天数智芯举办了“ Gitee AI &天数智芯创新应用大赛 ”。


这是一场面向全体开发者的科技比赛。由天数智芯负责算力资源支持，Gitee AI 提供展示创意和成果的平台，多家合作伙伴提供技术指导、传播支持。我们期待开发者们踊跃参与，贡献智慧，秀出实力。让我们一起激情燃烧，创享 AI 未来！

# 大赛时间线

![输入图片说明](images/2/%E9%80%92%E8%BF%9B%E5%85%B3%E7%B3%BB4%E9%A1%B9PPT%E5%9B%BE%E6%96%87%E9%A1%B5.jpg)

# 大赛说明 

## 报名参赛

1. 请点击下方“立即报名”按钮开始报名，在截止日期前提交报名信息后，大赛组委会将对参赛项目进行评审并公布入围结果。报名通过审核后，参赛者将在参赛期间获得 “天数智芯 天垓 100” 显卡免费使用权（算力券有效使用日期截止至10月1日）。

2. 报名截止日期：2024 年 7 月 20 日 24:00

3. 入围公布日期：2024 年 7 月 22 日


[活动报名入口](https://wj.qq.com/s2/14795837/1e1d/)


## 提交应用

1. 参赛者在截止日期前完成项目的开发。您可选择在 Gitee AI 直接 新建应用，使用 “天数智芯 天垓 100” 编写代码完成应用开发，编写代码完成应用开发，也可以 创建模型引擎
，将 AI 模型转为 API 服务， 然后在您的网站、小程序、APP 、扩展程序等调用 API。 应用完成后，请将作品体验的地址通过下方按钮提交。

2. 提交截止日期：2024 年 8 月 22 日 24:00

 _提交地址待开放_ 


## 网络投票

1. 大赛将公布参选项目的展示与投票页面，组委会将在 9 月 10 日结合投票排名以及专家评委意见，公布入围的 20 个应用。专家评委将为入围的 20 个应用提出优化意见，参赛者可根据意见继续优化作品，这将大大提高最后阶段的获奖机会哦！

2. 投票日期范围：2024 年 8 月 26 日 至 9 月 6 日

3. 入围公布日期：2024 年 9 月 10 日

 _入围应用界面待开放_ 


## 专家评审

1. 参赛者根据专家评委意见优化迭代作品，并在 9 月 20 日 15:00 前提交作品。专家评委将于 9 月 21 日评选出最终的结果并颁奖。

2. 迭代截止日期：2024 年 9 月 20 日 15:00

3. 大奖公布日期：2024 年 9 月 21 日

 _作品提交入口待开放_ 


# 作品要求

参赛者需要选择 Gitee AI 平台上天数智芯可适配的 AI 模型作为应用核心，根据自己的兴趣和创意，结合当下热点，选择不同的行业任务领域，设计出有价值的 AI 应用。

我们期待参赛者能够将 AIGC 模型的代码生成、图片生成、音视频生成、自然语言处理等生成式 AI 应用能力融入到更自然的、更具创造性的交互设计中，让每一位普通用户也能通过生成式 AI 应用来享用 AI 变革带来的创新性体验。

 **参考方向：** 

- 训练一个自己数字分身的语音克隆工具，能够实现语音对话聊天，例如给短视频配音，或是修改视频中的原声。

- 语音的方式生成 PlantUML 的各种图形，例如用语音生成一个 UML 序列图，并且可以随时进行修改调整。

- 利用情感分析和自然语言处理技术，开发一款智能助手应用，可以根据用户的聊天内容分析，并提供相应的建议和安慰。

- 基于用户的购物历史、浏览记录和兴趣爱好，利用机器学习算法提供个性化的购物推荐。

 _以上方案仅供参考，您可选择自己感兴趣的领域开发相关应用程序。_ 

 
# 大赛奖励

 **入围奖：** 颁发参赛证明

 **一等奖：** 1支队伍，奖金五万，颁发获奖证书

 **二等奖：** 2支队伍，奖金两万，颁发获奖证书

 **三等奖：** 3支队伍，奖金壹万，颁发获奖证书

 **优胜奖：** 9支队伍，赠送价值1000元礼品，颁发获奖证书

## 其他激励


（1）资源支持：Gitee AI 携手天数智芯，提供强大的平台支撑和算力资源，助力开发者实现创新梦想。

（2）技术支持：本次大赛的模型合作伙伴零一万物提供将想法落地孵化的技术支持，帮助参赛者降低开发难度。

（3）流量曝光：入选决赛的作品将获得媒体报道、流量扶持及市场推广机会。




# 评审规则

**注意事项** 

参赛者完成报名，即视为同意本活动相关规定；活动主办方对参赛作品享有使用权，用于展览、宣传、出版等非商业用途，不再另付报酬；活动最终解释权归主办方所有。

参赛者（团队）提交的作品信息中出现以下情况将被淘汰：

- 参赛报名信息作假；

- 在参赛过程中出现违反相关法律、法规的行为；

- 作品涉嫌抄袭，侵犯他人知识产权，直接套用开源代码等；

- 作品涉及政治危害、淫秽色情等违规违法内容。


# 评委阵容
吕坚平 | 天数智芯研究院院长

梁斌 | 天数智芯副总裁

红薯 | 开源中国CTO

林旅强 | 零一万物开源负责人

黄冰 | 中移科技（湖南）有限公司人工智能产品部经理

林家桢 | Gitee AI 专家顾问清华大学高性能计算所

![输入图片说明](images/2/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240708174030.jpg)